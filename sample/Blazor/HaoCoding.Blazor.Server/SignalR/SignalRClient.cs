﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;

namespace HaoCoding.Blazor.Server.SignalR
{
    public static class SendHubs
    {
        /// <summary>
        /// 调用hub方法
        /// </summary>
        /// <param name="methodName"></param>
        public static async Task Start(string methodName, params object[] args)
        {
            var hubConnection = new HubConnectionBuilder()
                .WithUrl("http://localhost:5000/default")
                .Build();

            await hubConnection.StartAsync();
            Console.WriteLine(hubConnection.State);
        }
    }
}
