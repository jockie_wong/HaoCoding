﻿using Microsoft.AspNetCore.Mvc;

namespace Bing.Extensions.Swashbuckle.Controllers
{
    /// <summary>
    /// Swagger 控制器
    /// </summary>
    [Route("api/swagger")]
    public class SwaggerController: Controller
    {
    }
}
