﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.IP
{
    using System;
    using System.IO;
    using System.Text;

    internal class Zip : IDisposable
    {
        #region 属性
        private UInt32 indexSet;
        private UInt32 indexEnd;
        private UInt32 indexCount;
        private UInt32 searchIndexSet;
        private UInt32 searchIndexEnd;
        private IndexInfo searchSet;
        private IndexInfo searchMid;
        private IndexInfo searchEnd;

        /// <summary>数据流</summary>
        public Stream Stream { get; set; }
        #endregion

        #region 构造
        /// <summary>析构</summary>
        ~Zip() { OnDispose(false); }

        /// <summary>销毁</summary>
        public void Dispose() => OnDispose(true);

        private void OnDispose(Boolean disposing)
        {
            if (Stream != null) Stream.Dispose();

            if (disposing) GC.SuppressFinalize(this);
        }
        #endregion

        #region 数据源
        public Zip SetStream(Stream stream)
        {
            var ms = new MemoryStream();

            var buf = new Byte[3];
            stream.Read(buf, 0, buf.Length);
            stream.Position -= 3;

            // 仅支持Gzip压缩，可用7z软件先压缩为gz格式
            if (buf[0] == 0x1F & buf[1] == 0x8B && buf[2] == 0x08)
                IOHelper.DecompressGZip(stream, ms);
            else
                IOHelper.CopyTo(stream, ms);

            ms.Position = 0;
            Stream = ms;

            indexSet = GetUInt32();
            indexEnd = GetUInt32();
            indexCount = ((indexEnd - indexSet) / 7u) + 1u;

            return this;
        }
        #endregion

        #region 方法
        public String GetAddress(UInt32 ip)
        {
            if (Stream == null) return "";

            searchIndexSet = 0u;
            searchIndexEnd = indexCount - 1u;

            while (true)
            {
                searchSet = IndexInfoAtPos(searchIndexSet);
                searchEnd = IndexInfoAtPos(searchIndexEnd);
                if (ip >= searchSet.IpSet && ip <= searchSet.IpEnd) break;

                if (ip >= searchEnd.IpSet && ip <= searchEnd.IpEnd) return ReadAddressInfoAtOffset(searchEnd.Offset);

                searchMid = IndexInfoAtPos((searchIndexEnd + searchIndexSet) / 2u);
                if (ip >= searchMid.IpSet && ip <= searchMid.IpEnd) return ReadAddressInfoAtOffset(searchMid.Offset);

                if (ip < searchMid.IpSet)
                    searchIndexEnd = (searchIndexEnd + searchIndexSet) / 2u;
                else
                    searchIndexSet = (searchIndexEnd + searchIndexSet) / 2u;
            }

            return ReadAddressInfoAtOffset(searchSet.Offset);
        }

        private String ReadAddressInfoAtOffset(UInt32 offset1)
        {
            Stream.Position = offset1 + 4;
            var tag = GetTag();
            String addr;
            String area;
            if (tag == 1)
            {
                Stream.Position = GetOffset();
                tag = GetTag();
                if (tag == 2)
                {
                    var offset = GetOffset();
                    area = ReadArea();
                    Stream.Position = offset;
                    addr = ReadString();
                }
                else
                {
                    Stream.Position -= 1;
                    addr = ReadString();
                    area = ReadArea();
                }
            }
            else
            {
                if (tag == 2)
                {
                    var offset = GetOffset();
                    area = ReadArea();
                    Stream.Position = offset;
                    addr = ReadString();
                }
                else
                {
                    Stream.Position -= 1;
                    addr = ReadString();
                    area = ReadArea();
                }
            }

            return (addr + " " + area).Trim();
        }

        private UInt32 GetOffset()
        {
            return BitConverter.ToUInt32(new Byte[]
                {
                    (Byte)Stream.ReadByte(),
                    (Byte)Stream.ReadByte(),
                    (Byte)Stream.ReadByte(),
                    0
                }, 0);
        }

        private String ReadArea()
        {
            var tag = GetTag();
            if (tag == 1 || tag == 2)
                Stream.Position = GetOffset();
            else
                Stream.Position -= 1;

            return ReadString();
        }

        private String ReadString()
        {
            var k = 0;
            var buf = new Byte[256];
            buf[k] = (Byte)Stream.ReadByte();
            while (buf[k] != 0)
            {
                k += 1;
                buf[k] = (Byte)Stream.ReadByte();
            }

            var str = Encoding.GetEncoding("GB2312").GetString(buf).Trim().Trim('\0').Trim();
            if (str == "CZ88.NET") return null;
            return str;
        }

        private Byte GetTag() => (Byte)Stream.ReadByte();

        private IndexInfo IndexInfoAtPos(UInt32 index_Pos)
        {
            var inf = new IndexInfo();
            Stream.Position = indexSet + (7u * index_Pos);
            inf.IpSet = GetUInt32();
            inf.Offset = GetOffset();
            Stream.Position = inf.Offset;
            inf.IpEnd = GetUInt32();
            return inf;
        }

        private UInt32 GetUInt32()
        {
            var array = new Byte[4];
            Stream.Read(array, 0, 4);
            return BitConverter.ToUInt32(array, 0);
        }
        #endregion

        /// <summary>索引结构</summary>
        private class IndexInfo
        {
            public UInt32 IpSet;
            public UInt32 IpEnd;
            public UInt32 Offset;
        }
    }
}
