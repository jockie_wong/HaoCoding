﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Caching
{
    using System;
    using System.Collections.Generic;

    /// <summary>生产者消费者接口</summary>
    /// <typeparam name="T"></typeparam>
    public interface IProducerConsumer<T>
    {
        /// <summary>生产添加</summary>
        /// <param name="values"></param>
        /// <returns></returns>
        Int32 Add(IEnumerable<T> values);

        /// <summary>消费获取</summary>
        /// <param name="count"></param>
        /// <returns></returns>
        IEnumerable<T> Take(Int32 count = 1);
    }
}
