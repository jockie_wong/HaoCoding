﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Collections
{
    using System;
    using System.Collections;
    using System.Collections.Concurrent;
    using System.Collections.Generic;

    /// <summary>并行哈希集合</summary>
    /// <remarks>
    /// 主要用于频繁添加删除而又要遍历的场合
    /// </remarks>
    public class ConcurrentHashSet<T> : IEnumerable<T>
    {
        private ConcurrentDictionary<T, Byte> _dic = new ConcurrentDictionary<T, Byte>();

        /// <summary>是否空集合</summary>
        public Boolean IsEmpty => _dic.IsEmpty;

        /// <summary>元素个数</summary>
        public Int32 Count => _dic.Count;

        /// <summary>是否包含元素</summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean Contain(T item) => _dic.ContainsKey(item);

        /// <summary>尝试添加</summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean TryAdd(T item) => _dic.TryAdd(item, 0);

        /// <summary>尝试删除</summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public Boolean TryRemove(T item) => _dic.TryRemove(item, out var b);

        #region IEnumerable<T> 成员
        IEnumerator<T> IEnumerable<T>.GetEnumerator() => _dic.Keys.GetEnumerator();
        #endregion

        #region IEnumerable 成员
        IEnumerator IEnumerable.GetEnumerator() => _dic.Keys.GetEnumerator();
        #endregion
    }
}
