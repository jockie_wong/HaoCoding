﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding
{
    using System;
    using System.Runtime.InteropServices;

    /// <summary>运行时</summary>
    public static class Runtime
    {
        #region 控制台
        private static Boolean? _isConsole;
        /// <summary>是否控制台。用于判断是否可以执行一些控制台操作。</summary>
        public static Boolean IsConsole
        {
            get
            {
                if (_isConsole != null) return _isConsole.Value;

                try
                {
                    var flag = Console.CursorVisible;
                    _isConsole = true;
                }
                catch
                {
                    _isConsole = false;
                }

                return _isConsole.Value;
            }
        }
        #endregion

        #region 系统特性
        /// <summary>是否Mono环境</summary>
        public static Boolean Mono { get; } = Type.GetType("Mono.Runtime") != null;

#if __CORE__
        /// <summary>是否Web环境</summary>
        public static Boolean IsWeb => false;

        /// <summary>是否Windows环境</summary>
        public static Boolean Windows => RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        /// <summary>是否Linux环境</summary>
        public static Boolean Linux => RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        /// <summary>是否OSX环境</summary>
        public static Boolean OSX => RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
#else
        /// <summary>是否Web环境</summary>
        public static Boolean IsWeb => !String.IsNullOrEmpty(System.Web.HttpRuntime.AppDomainAppId);

        /// <summary>是否Windows环境</summary>
        public static Boolean Windows { get; } = Environment.OSVersion.Platform <= PlatformID.WinCE;

        /// <summary>是否Linux环境</summary>
        public static Boolean Linux => false;

        /// <summary>是否OSX环境</summary>
        public static Boolean OSX => false;
#endif
        #endregion
    }
}
