﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Data
{
    /// <summary>经纬度坐标</summary>
    public class GeoPoint
    {
        #region 属性
        /// <summary>经度</summary>
        public Double Longitude { get; set; }

        /// <summary>纬度</summary>
        public Double Latitude { get; set; }
        #endregion

        #region 构造
        /// <summary>经纬度坐标</summary>
        public GeoPoint() { }

        /// <summary>经纬度坐标</summary>
        /// <param name="location"></param>
        public GeoPoint(String location)
        {
            if (!location.IsNullOrEmpty())
            {
                var ss = location.Split(",");
                if (ss.Length >= 2)
                {
                    Longitude = ss[0].ToDouble();
                    Latitude = ss[1].ToDouble();
                }
            }
        }
        #endregion

        /// <summary>已重载</summary>
        /// <returns></returns>
        public override String ToString() => $"{Longitude},{Latitude}";
    }
}