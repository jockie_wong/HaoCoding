﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Reflection;
using HaoCoding.Reflection;

namespace HaoCoding
{
    /// <summary>弱引用Action</summary>
    /// <remarks>
    /// 常见的事件和委托，都包括两部分：对象和方法，当然如果委托到静态方法上，对象是为空的。
    /// 如果把事件委托到某个对象的方法上，同时就间接的引用了这个对象，导致其一直无法被回收，从而造成内存泄漏。
    /// 弱引用Action，原理就是把委托拆分，然后弱引用对象部分，需要调用委托的时候，再把对象“拉”回来，如果被回收了，就没有必要再调用它的方法了。
    /// </remarks>
    /// <typeparam name="TArgs"></typeparam>
    public class WeakAction<TArgs>
    {
        #region 属性
        /// <summary>目标对象。弱引用，使得调用方对象可以被GC回收</summary>
        private readonly WeakReference target;

        /// <summary>委托方法</summary>
        private readonly MethodBase method;

        /// <summary>经过包装的新的委托</summary>
        private readonly Action<TArgs> handler;

        /// <summary>取消注册的委托</summary>
        private Action<Action<TArgs>> unHandler;

        /// <summary>是否只使用一次，如果只使用一次，执行委托后马上取消注册</summary>
        private readonly Boolean once;
        #endregion

        #region 扩展属性
        /// <summary>是否可用</summary>
        public Boolean IsAlive
        {
            get
            {
                var target = this.target;
                if (target == null && method.IsStatic) return true;

                return target != null && target.IsAlive;
            }
        }
        #endregion

        #region 构造
        /// <summary>实例化</summary>
        /// <param name="target">目标对象</param>
        /// <param name="method">目标方法</param>
        public WeakAction(Object target, MethodInfo method) : this(target, method, null, false) { }

        /// <summary>实例化</summary>
        /// <param name="target">目标对象</param>
        /// <param name="method">目标方法</param>
        /// <param name="unHandler">取消注册回调</param>
        /// <param name="once">是否一次性事件</param>
        public WeakAction(Object target, MethodInfo method, Action<Action<TArgs>> unHandler, Boolean once)
        {
            if (target != null)
            {
                this.target = new WeakReference(target);
            }
            else
            {
                if (!method.IsStatic) throw new InvalidOperationException("非法事件，没有指定类实例且不是静态方法！");
            }

            this.method = method;
            handler = Invoke;
            this.unHandler = unHandler;
            this.once = once;
        }

        /// <summary>实例化</summary>
        /// <param name="handler">事件处理器</param>
        public WeakAction(Delegate handler) : this(handler.Target, handler.Method, null, false) { }

        /// <summary>使用事件处理器、取消注册回调、是否一次性事件来初始化</summary>
        /// <param name="handler">事件处理器</param>
        /// <param name="unHandler">取消注册回调</param>
        /// <param name="once">是否一次性事件</param>
        public WeakAction(Delegate handler, Action<Action<TArgs>> unHandler, Boolean once) : this(handler.Target, handler.Method, unHandler, once) { }
        #endregion

        #region 方法
        /// <summary>调用委托</summary>
        /// <param name="e"></param>
        public void Invoke(TArgs e)
        {
            //if (!Target.IsAlive) return;
            // Keep in mind that，不要用上面的写法，因为判断可能通过，但是接着就被GC回收了，如果判断Target，则会增加引用
            Object target = null;
            if (this.target == null)
            {
                if (method.IsStatic) Reflect.Invoke(null, method, e);
            }
            else
            {
                target = this.target.Target;
                if (target != null)
                {
                    // 优先使用委托
                    if (method is MethodInfo mi)
                        mi.As<Action<TArgs>>(target).Invoke(e);
                    else
                        target.Invoke(method, e);
                }
            }

            // 调用方已被回收，或者该事件只使用一次，则取消注册
            if (((this.target != null && target == null) || once) && unHandler != null)
            {
                unHandler(handler);
                unHandler = null;
            }
        }

        /// <summary>把弱引用事件处理器转换为普通事件处理器</summary>
        /// <param name="handler"></param>
        /// <returns></returns>
        public static implicit operator Action<TArgs>(WeakAction<TArgs> handler) => handler.handler;
        #endregion

        #region 辅助
        /// <summary>已重载</summary>
        /// <returns></returns>
        public override String ToString()
        {
            if (method == null) return base.ToString();

            if (method.DeclaringType != null)
                return String.Format("{0}.{1}", method.DeclaringType.Name, method.Name);
            else
                return method.Name;
        }
        #endregion
    }
}