﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;

namespace HaoCoding.Expressions
{
    internal class IndexInfoResult
    {
        public IndexInfoResult(String indexMark)
        {
            Mark = indexMark;
            IndexInfos = new List<IndexInfo>();
        }

        public String Mark { get; set; }

        public IList<IndexInfo> IndexInfos { get; private set; }
    }

    internal class IndexInfo
    {
        public IndexInfo(String stockCode, Int32 value)
        {
            StockCode = stockCode;
            Value = value;
        }

        public IndexInfo(String code)
        {
            StockCode = code;
        }

        public String StockCode { get; set; }

        public Int32? Value { get; set; }
    }
}