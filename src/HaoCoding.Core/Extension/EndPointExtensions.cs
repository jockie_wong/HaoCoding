﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System.Collections.Generic;
using System.Net;

namespace System
{
    /// <summary>网络结点扩展</summary>
    public static class EndPointExtensions
    {
        public static String ToAddress(this EndPoint endpoint)
        {
            return ((IPEndPoint)endpoint).ToAddress();
        }

        public static String ToAddress(this IPEndPoint endpoint)
        {
            return String.Format("{0}:{1}", endpoint.Address, endpoint.Port);
        }

        public static IPEndPoint ToEndPoint(this String address)
        {
            var array = address.Split(new String[] { ":" }, StringSplitOptions.RemoveEmptyEntries);
            if (array.Length != 2)
            {
                throw new Exception("Invalid endpoint address: " + address);
            }
            var ip = IPAddress.Parse(array[0]);
            var port = Int32.Parse(array[1]);
            return new IPEndPoint(ip, port);
        }

        public static IEnumerable<IPEndPoint> ToEndPoints(this String addresses)
        {
            var array = addresses.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            var list = new List<IPEndPoint>();
            foreach (var item in array)
            {
                list.Add(item.ToEndPoint());
            }
            return list;
        }
    }
}