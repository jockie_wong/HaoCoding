﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Extension
{
    using System;
    using System.Reflection;
    using HaoCoding.Log;
    using HaoCoding.Reflection;

    internal class SpeakProvider
    {
        private static String typeName = "System.Speech.Synthesis.SpeechSynthesizer";
        private Type _type;

        public SpeakProvider()
        {
            try
            {
                // 新版系统内置
                if (Environment.OSVersion.Version.Major >= 6)
                    Assembly.Load("System.Speech, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35");

                _type = typeName.GetTypeEx(true);
            }
            catch (Exception ex)
            {
                XTrace.WriteException(ex);
            }
        }

        private Object synth;

        private void EnsureSynth()
        {
            if (synth == null)
            {
                try
                {
                    synth = _type.CreateInstance(new Object[0]);
                    synth.Invoke("SetOutputToDefaultAudioDevice", new Object[0]);
                }
                catch (Exception ex)
                {
                    XTrace.WriteException(ex);
                    _type = null;
                }
            }
        }

        public void Speak(String value)
        {
            if (_type == null) return;

            EnsureSynth();
            if (synth != null) synth.Invoke("Speak", value);
        }

        public void SpeakAsync(String value)
        {
            if (_type == null) return;

            EnsureSynth();
            if (synth != null) synth.Invoke("SpeakAsync", value);
        }

        /// <summary>
        /// 停止话音播报
        /// </summary>
        public void SpeakAsyncCancelAll()
        {
            if (_type == null) return;

            EnsureSynth();
            if (synth != null) synth.Invoke("SpeakAsyncCancelAll");
        }
    }
}