﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Log
{
    /// <summary>依托于动作的日志类</summary>
    public class ActionLog : Logger
    {
        /// <summary>方法</summary>
        public Action<String, Object[]> Method { get; set; }

        /// <summary>使用指定方法否则动作日志</summary>
        /// <param name="action"></param>
        public ActionLog(Action<String, Object[]> action) { Method = action; }

        /// <summary>写日志</summary>
        /// <param name="level"></param>
        /// <param name="format"></param>
        /// <param name="args"></param>
        protected override void OnWrite(LogLevel level, String format, params Object[] args)
        {
            if (Method != null) Method(format, args);
        }

        /// <summary>已重载</summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Method + "";
        }
    }
}