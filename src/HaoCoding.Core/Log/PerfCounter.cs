﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using HaoCoding.Threading;

namespace HaoCoding.Log
{
    /// <summary>性能计数器。次数、TPS、平均耗时</summary>
    public class PerfCounter : DisposeBase, ICounter
    {
        #region 属性
        /// <summary>是否启用。默认true</summary>
        public Boolean Enable { get; set; } = true;

        private Int64 _value;
        /// <summary>数值</summary>
        public Int64 Value => _value;

        private Int64 _times;
        /// <summary>次数</summary>
        public Int64 Times => _times;

        /// <summary>耗时，单位us</summary>
        private Int64 _totalCost;
        #endregion

        #region 构造
        /// <summary>销毁</summary>
        /// <param name="disposing"></param>
        protected override void OnDispose(Boolean disposing)
        {
            base.OnDispose(disposing);

            _timer.TryDispose();
        }
        #endregion

        #region 核心方法
        /// <summary>增加</summary>
        /// <param name="value">增加的数量</param>
        /// <param name="usCost">耗时，单位us</param>
        public void Increment(Int64 value, Int64 usCost)
        {
            if (!Enable) return;

            // 累加总次数和总数值
            Interlocked.Add(ref _value, value);
            Interlocked.Increment(ref _times);
            if (usCost > 0) Interlocked.Add(ref _totalCost, usCost);

            if (_timer == null)
            {
                lock (this)
                {
                    if (_timer == null) _timer = new TimerX(DoWork, null, Interval, Interval);
                }
            }
        }
        #endregion

        #region 采样
        /// <summary>采样间隔，默认1000毫秒</summary>
        public Int32 Interval { get; set; } = 1000;

        /// <summary>持续采样时间，默认60秒</summary>
        public Int32 Duration { get; set; } = 60;

        /// <summary>当前速度</summary>
        public Int64 Speed { get; private set; }

        /// <summary>最大速度</summary>
        public Int64 MaxSpeed => _quSpeed.Max();

        /// <summary>最后一个采样周期的平均耗时，单位us</summary>
        public Int64 Cost { get; private set; }

        /// <summary>持续采样时间内的最大平均耗时，单位us</summary>
        public Int64 MaxCost => _quCost.Max();

        private Int64[] _quSpeed = new Int64[60];
        private Int64[] _quCost = new Int64[60];
        private Int32 _queueIndex = -1;

        private TimerX _timer;
        private Stopwatch _sw;
        private Int64 _lastValue;
        private Int64 _lastTimes;
        private Int64 _lastCost;

        /// <summary>定期采样，保存最近60组到数组队列里面</summary>
        /// <param name="state"></param>
        private void DoWork(Object state)
        {
            // 计算采样次数
            var len = Duration * 1000 / Interval;

            if (_quSpeed.Length != len) _quSpeed = new Int64[len];
            if (_quCost.Length != len) _quCost = new Int64[len];

            // 计算速度
            var sp = 0L;
            var cc = 0L;
            if (_sw == null)
                _sw = Stopwatch.StartNew();
            else
            {
                var ms = _sw.Elapsed.TotalMilliseconds;
                _sw.Restart();

                sp = (Int64)((Value - _lastValue) * 1000 / ms);
            }

            _lastValue = Value;

            // 计算本周期平均耗时
            // 本周期内的总耗时除以总次数，得到本周期平均耗时
            var ts = Times - _lastTimes;
            cc = ts == 0 ? Cost : ((_totalCost - _lastCost) / ts);

            _lastTimes = Times;
            _lastCost = _totalCost;

            Speed = sp;
            Cost = cc;

            // 进入队列
            _queueIndex++;
            if (_queueIndex < 0 || _queueIndex >= len) _queueIndex = 0;
            _quSpeed[_queueIndex] = sp;
            _quCost[_queueIndex] = cc;
        }
        #endregion

        #region 辅助
        /// <summary>已重载。输出统计信息</summary>
        /// <returns></returns>
        public override String ToString()
        {
            if (Cost >= 1000)
                return "{0:n0}/{1:n0}/{2:n0}tps/{3:n0}/{4:n0}ms".F(Times, MaxSpeed, Speed, MaxCost / 1000, Cost / 1000);
            if (Cost > 0)
                return "{0:n0}/{1:n0}/{2:n0}tps/{3:n0}/{4:n0}us".F(Times, MaxSpeed, Speed, MaxCost, Cost);
            else
                return "{0:n0}/{1:n0}/{2:n0}tps".F(Times, MaxSpeed, Speed);
        }
        #endregion
    }
}