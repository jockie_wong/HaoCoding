﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using HaoCoding.Data;

namespace HaoCoding.Messaging
{
    /// <summary>收到消息时的事件参数</summary>
    public class MessageEventArgs : EventArgs
    {
        #region 属性
        /// <summary>数据包</summary>
        public Packet Packet { get; set; }

        /// <summary>消息</summary>
        public IMessage Message { get; set; }

        /// <summary>用户数据。比如远程地址等</summary>
        public Object UserState { get; set; }
        #endregion

        #region 方法
        #endregion
    }
}