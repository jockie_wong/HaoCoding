﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using HaoCoding.Reflection;

namespace HaoCoding.Model
{
    /// <summary>用于创建对象的工厂接口</summary>
    /// <typeparam name="T"></typeparam>
    public interface IFactory<T>
    {
        /// <summary>创建对象实例</summary>
        /// <param name="args"></param>
        /// <returns></returns>
        T Create(Object args = null);
    }

    /// <summary>反射创建对象的工厂</summary>
    /// <typeparam name="T"></typeparam>
    public class Factory<T> : IFactory<T>
    {
        /// <summary>创建对象实例</summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public virtual T Create(Object args = null) => (T)typeof(T).CreateInstance();
    }
}