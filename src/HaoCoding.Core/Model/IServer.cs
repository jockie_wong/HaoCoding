﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Model
{
    /// <summary>服务接口。</summary>
    /// <remarks>服务代理XAgent可以附加代理实现了IServer接口的服务。</remarks>
    public interface IServer
    {
        /// <summary>开始</summary>
        void Start();

        /// <summary>停止</summary>
        /// <param name="reason">关闭原因。便于日志分析</param>
        void Stop(String reason);
    }
}