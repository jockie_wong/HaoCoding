﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Net;
using HaoCoding.Data;

namespace HaoCoding.Net
{
    /// <summary>收到数据时的事件参数</summary>
    public class ReceivedEventArgs : EventArgs, IData
    {
        #region 属性
        /// <summary>数据包</summary>
        public Packet Packet { get; set; }

        /// <summary>远程地址</summary>
        public IPEndPoint Remote { get; set; }

        /// <summary>解码后的消息</summary>
        public Object Message { get; set; }

        /// <summary>用户数据</summary>
        public Object UserState { get; set; }
        #endregion
    }
}