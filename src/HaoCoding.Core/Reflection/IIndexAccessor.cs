﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Reflection
{
    /// <summary>
    /// 索引器接访问口。
    /// 该接口用于通过名称快速访问对象属性或字段（属性优先）。
    /// </summary>
    //[Obsolete("=>IIndex")]
    public interface IIndexAccessor
    {
        /// <summary>获取/设置 指定名称的属性或字段的值</summary>
        /// <param name="name">名称</param>
        /// <returns></returns>
        Object this[String name] { get; set; }
    }
}