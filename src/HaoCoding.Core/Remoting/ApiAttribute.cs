﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Remoting
{
    /// <summary>标识Api</summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class ApiAttribute : Attribute
    {
        /// <summary>名称</summary>
        public String Name { get; set; }

        /// <summary>实例化</summary>
        /// <param name="name"></param>
        public ApiAttribute(String name) => Name = name;
    }
}