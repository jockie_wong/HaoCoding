﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Remoting
{
    /// <summary>远程调用异常</summary>
    public class ApiException : Exception
    {
        /// <summary>代码</summary>
        public Int32 Code { get; set; }

        /// <summary>实例化远程调用异常</summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public ApiException(Int32 code, String message) : base(message) => Code = code;

        /// <summary>实例化远程调用异常</summary>
        /// <param name="code"></param>
        /// <param name="ex"></param>
        public ApiException(Int32 code, Exception ex) : base(ex.Message, ex) => Code = code;
    }
}