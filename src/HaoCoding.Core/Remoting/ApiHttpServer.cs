﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using HaoCoding.Http;
using HaoCoding.Net;

namespace HaoCoding.Remoting
{
    class ApiHttpServer : ApiNetServer
    {
        #region 属性
        private String rawUrl;
        #endregion

        public ApiHttpServer()
        {
            Name = "Http";

            ProtocolType = NetType.Http;
        }

        /// <summary>初始化</summary>
        /// <param name="config"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        public override Boolean Init(Object config, IApiHost host)
        {
            Host = host;

            var uri = config as NetUri;
            Port = uri.Port;

            rawUrl = uri + "";

            // Http封包协议
            Add<HttpCodec>();

            host.Handler = new ApiHttpHandler { Host = host };
            host.Encoder = new HttpEncoder();

            return true;
        }
    }

    class ApiHttpHandler : ApiHandler
    {
    }
}