﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;

namespace HaoCoding.Remoting
{
    /// <summary>控制器上下文</summary>
    public class ControllerContext
    {
        /// <summary>控制器实例</summary>
        public Object Controller { get; set; }

        /// <summary>处理动作</summary>
        public ApiAction Action { get; set; }

        /// <summary>真实动作名称</summary>
        public String ActionName { get; set; }

        /// <summary>会话</summary>
        public IApiSession Session { get; set; }

        /// <summary>请求</summary>
        public Object Request { get; set; }

        /// <summary>请求参数</summary>
        public IDictionary<String, Object> Parameters { get; set; }

        /// <summary>获取或设置操作方法参数。</summary>
        public virtual IDictionary<String, Object> ActionParameters { get; set; }

        /// <summary>获取或设置由操作方法返回的结果。</summary>
        public Object Result { get; set; }

        /// <summary>获取或设置在操作方法的执行过程中发生的异常（如果有）。</summary>
        public virtual Exception Exception { get; set; }

        /// <summary>获取或设置一个值，该值指示是否处理异常。</summary>
        public Boolean ExceptionHandled { get; set; }

        /// <summary>实例化</summary>
        public ControllerContext() { }

        ///// <summary>拷贝实例化</summary>
        ///// <param name="context"></param>
        //public ControllerContext(ControllerContext context)
        //{
        //    Controller = context.Controller;
        //    Action = context.Action;
        //    ActionName = context.ActionName;
        //    Session = context.Session;
        //    Request = context.Request;
        //    Parameters = context.Parameters;
        //}

        [ThreadStatic]
        private static ControllerContext _current;
        /// <summary>当前线程上下文</summary>
        public static ControllerContext Current { get { return _current; } set { _current = value; } }

        /// <summary>重置为默认状态</summary>
        public void Reset()
        {
            Controller = null;
            Action = null;
            ActionName = null;
            Session = null;
            Request = null;
            Parameters = null;
            ActionParameters = null;
            Result = null;
            Exception = null;
            ExceptionHandled = false;
        }
    }
}