﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;

namespace HaoCoding.Remoting
{
    /// <summary>定义操作筛选器中使用的方法。</summary>
    public interface IActionFilter
    {
        /// <summary>在执行操作方法之前调用。</summary>
        /// <param name="filterContext"></param>
        void OnActionExecuting(ControllerContext filterContext);

        /// <summary>在执行操作方法后调用。</summary>
        /// <param name="filterContext"></param>
        void OnActionExecuted(ControllerContext filterContext);
    }
}