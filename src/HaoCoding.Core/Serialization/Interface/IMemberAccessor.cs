﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.IO;
using System.Reflection;

namespace HaoCoding.Serialization
{
    /// <summary>成员序列化访问器。接口实现者可以在这里完全自定义序列化行为</summary>
    public interface IMemberAccessor
    {
        /// <summary>从数据流中读取消息</summary>
        /// <param name="fm">序列化</param>
        /// <param name="member">成员</param>
        /// <returns>是否成功</returns>
        Boolean Read(IFormatterX fm, MemberInfo member);

        /// <summary>把消息写入到数据流中</summary>
        /// <param name="fm">序列化</param>
        /// <param name="member">成员</param>
        void Write(IFormatterX fm, MemberInfo member);
    }
}