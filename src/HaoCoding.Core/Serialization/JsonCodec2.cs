﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;
using HaoCoding.Data;
using HaoCoding.Model;
using HaoCoding.Net.Handlers;

namespace HaoCoding.Serialization
{
    /// <summary>Json编码解码器</summary>
    public class JsonCodec2 : Handler
    {
        /// <summary>对象转Json</summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public override Object Write(IHandlerContext context, Object message)
        {
            if (message is IDictionary<String, Object> entity) return new Packet(entity.ToJson().GetBytes());

            return message;
        }

        /// <summary>Json转对象</summary>
        /// <param name="context"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public override Object Read(IHandlerContext context, Object message)
        {
            if (message is Packet pk) message = pk.ToStr();
            if (message is String str) return new JsonParser(str).Decode();

            return message;
        }
    }
}