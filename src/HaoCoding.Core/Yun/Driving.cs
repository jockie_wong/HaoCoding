﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;

namespace HaoCoding.Yun
{
    /// <summary>驾车距离和时间</summary>
    public class Driving
    {
        /// <summary>距离。单位千米</summary>
        public Int32 Distance { get; set; }

        /// <summary>路线耗时。单位秒</summary>
        public Int32 Duration { get; set; }
    }
}