﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using HaoCoding.Xml;

namespace HaoCoding.DingTalk
{
    /// <summary>钉钉参数配置</summary>
    [DisplayName("钉钉参数配置")]
    [XmlConfigFile(@"Config\DingTalk.config", 15000)]
    class DingTalkSetting
    {
    }
}
