﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Printer.UnicodeToZPL
{
    public enum CompressCode
    {
        G = 1,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T,
        U,
        V,
        W,
        X,
        Y,
        g,
        h = 40,
        i = 60,
        j = 80,
        k = 100,
        l = 120,
        m = 140,
        n = 160,
        o = 180,
        p = 200,
        q = 220,
        r = 240,
        s = 260,
        t = 280,
        u = 300,
        v = 320,
        w = 340,
        x = 360,
        y = 380,
        z = 400
    }
}
