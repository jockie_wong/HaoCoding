﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Printer.UnicodeToZPL
{
    public enum TextDirection
    {
        零度,
        四十五度 = 45,
        九十度 = 90,
        一百八十度 = 180,
        二百七十度 = 270
    }
}
