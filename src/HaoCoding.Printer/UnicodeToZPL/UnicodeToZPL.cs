﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Text;
using System.Threading;

namespace HaoCoding.Printer.UnicodeToZPL
{
    public class UnicodeToZPL
    {
        public static event Action<Image> OnViewImageCallBack
        {
            add
            {
                Action<Image> action = action_0;
                Action<Image> action2;
                do
                {
                    action2 = action;
                    Action<Image> value2 = (Action<Image>)Delegate.Combine(action2, value);
                    action = Interlocked.CompareExchange(ref action_0, value2, action2);
                }
                while (action != action2);
            }
            remove
            {
                Action<Image> action = action_0;
                Action<Image> action2;
                do
                {
                    action2 = action;
                    Action<Image> value2 = (Action<Image>)Delegate.Remove(action2, value);
                    action = Interlocked.CompareExchange(ref action_0, value2, action2);
                }
                while (action != action2);
            }
        }

        public static string UnCompressZPL(string content, string name, Font font, TextDirection textDirection)
        {
            Bitmap bitmap = smethod_1(content, font, textDirection);
            if (textDirection != TextDirection.零度)
            {
                bitmap = smethod_2(bitmap, (int)textDirection);
            }
            action_0?.Invoke(bitmap);
            string text = smethod_3(bitmap);
            string text2 = ((bitmap.Size.Width / 8 + ((bitmap.Size.Width % 8 == 0) ? 0 : 1)) * bitmap.Size.Height).ToString();
            string text3 = (bitmap.Size.Width / 8 + ((bitmap.Size.Width % 8 == 0) ? 0 : 1)).ToString();
            return string.Format("~DG{0}.GRF,{1},{2},{3}^XG{0}", new object[]
            {
                name,
                text2,
                text3,
                text
            });
        }

        public static string CompressZPL(string content, string name, Font font, TextDirection textDirection)
        {
            Bitmap bitmap = smethod_1(content, font, textDirection);
            if (textDirection != TextDirection.零度)
            {
                bitmap = smethod_2(bitmap, (int)textDirection);
            }
            action_0?.Invoke(bitmap);
            string text = smethod_3(bitmap);
            text = Class0.smethod_0(text);
            string text2 = ((bitmap.Size.Width / 8 + ((bitmap.Size.Width % 8 == 0) ? 0 : 1)) * bitmap.Size.Height).ToString();
            string text3 = (bitmap.Size.Width / 8 + ((bitmap.Size.Width % 8 == 0) ? 0 : 1)).ToString();
            return string.Format("~DG{0}.GRF,{1},{2},{3}^XG{0}", new object[]
            {
                name,
                text2,
                text3,
                text
            });
        }

        private static Bitmap smethod_1(string string_0, Font font_0, TextDirection textDirection_0)
        {
            StringFormat stringFormat = new StringFormat(StringFormatFlags.NoClip);
            Bitmap bitmap = new Bitmap(1, 1);
            Graphics graphics = Graphics.FromImage(bitmap);
            SizeF sizeF = graphics.MeasureString(string_0, font_0, PointF.Empty, stringFormat);
            int width = (int)(sizeF.Width + 1f);
            int height = (int)(sizeF.Height + 1f);
            Rectangle rectangle = new Rectangle(0, 0, width, height);
            bitmap.Dispose();
            bitmap = new Bitmap(width, height);
            graphics = Graphics.FromImage(bitmap);
            graphics.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
            graphics.FillRectangle(new SolidBrush(Color.White), rectangle);
            graphics.DrawString(string_0, font_0, Brushes.Black, rectangle, stringFormat);
            return bitmap;
        }

        private static Bitmap smethod_2(Bitmap bitmap_0, int int_0)
        {
            int_0 %= 360;
            double num = int_0 * 3.1415926535897931 / 180.0;
            double num2 = Math.Cos(num);
            double num3 = Math.Sin(num);
            int width = bitmap_0.Width;
            int height = bitmap_0.Height;
            int num4 = (int)Math.Max(Math.Abs(width * num2 - height * num3), Math.Abs(width * num2 + height * num3));
            int num5 = (int)Math.Max(Math.Abs(width * num3 - height * num2), Math.Abs(width * num3 + height * num2));
            Bitmap bitmap = new Bitmap(num4, num5);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.InterpolationMode = InterpolationMode.Bilinear;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            Point point = new Point((num4 - width) / 2, (num5 - height) / 2);
            Rectangle rect = new Rectangle(point.X, point.Y, width, height);
            Point point2 = new Point(rect.X + rect.Width / 2, rect.Y + rect.Height / 2);
            graphics.TranslateTransform(point2.X, point2.Y);
            graphics.RotateTransform(360 - int_0);
            graphics.TranslateTransform(-point2.X, -point2.Y);
            graphics.DrawImage(bitmap_0, rect);
            graphics.ResetTransform();
            graphics.Save();
            graphics.Dispose();
            return bitmap;
        }

        private unsafe static string smethod_3(Bitmap bitmap_0)
        {
            StringBuilder stringBuilder = new StringBuilder();
            BitmapData bitmapData = bitmap_0.LockBits(new Rectangle(0, 0, bitmap_0.Width, bitmap_0.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
            byte* ptr = (byte*)bitmapData.Scan0.ToPointer();
            for (int i = 0; i < bitmapData.Height; i++)
            {
                int num = 0;
                int num2 = 0;
                for (int j = 0; j < bitmapData.Width; j++)
                {
                    float num3 = 0.11f * ptr[i * bitmapData.Stride + j * 3] + 0.59f * ptr[i * bitmapData.Stride + j * 3 + 1] + 0.3f * ptr[i * bitmapData.Stride + j * 3 + 2];
                    num2 *= 2;
                    if (num3 < 187f)
                    {
                        num2++;
                    }
                    num++;
                    if (j == bitmapData.Width - 1 && num < 8)
                    {
                        string value = (num2 * (2 ^ 8 - num)).ToString("X").PadLeft(2, '0');
                        stringBuilder.Append(value);
                        num2 = 0;
                        num = 0;
                    }
                    if (num >= 8)
                    {
                        string value2 = num2.ToString("X").PadLeft(2, '0');
                        stringBuilder.Append(value2);
                        num2 = 0;
                        num = 0;
                    }
                }
            }
            bitmap_0.UnlockBits(bitmapData);
            return stringBuilder.ToString();
        }

        private static Action<Image> action_0;
    }
}
