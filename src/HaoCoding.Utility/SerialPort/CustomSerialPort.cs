﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.SerialPort
{
#if !NET45
    using System;
    using System.Collections.Generic;
    using System.Threading;
    using RJCP.IO.Ports;

    /// <summary>
    /// 一个增强的自定义串口类，实现协议无关的数据帧完整接收功能，支持跨平台使用
    /// 使用SerialPortStream基础类库
    /// 采用接收超时机制，两次通讯之间需有一定的间隔时间
    /// 如间隔时间太短，则需要拆包
    /// </summary>
    public class CustomSerialPort
    {
        /// <summary>
        /// 把接收到消息后需要触发的时间绑定到此事件
        /// </summary>
        public event CustomSerialPortReceivedEventHandle ReceivedEvent;

        public SerialPortStream Serial = null;

        /// <summary>
        /// 获取或设置通信端口，包括但不限于所有可用的 COM 端口
        /// </summary>
        public string PortName
        {
            get { return Serial.PortName; }
            set { Serial.PortName = value; }
        }

        /// <summary>
        /// 获取或设置串行波特率,指每秒传送的比特(bit)数
        /// </summary>
        public int BaudRate
        {
            get { return Serial.BaudRate; }
            set { Serial.BaudRate = value; }
        }

        /// <summary>
        /// 获取或设置奇偶校验检查协议,枚举，None：没有校验为，Odd：奇校验，Even：偶检验，Space：总为0，Mark：总为1
        /// </summary>
        public Parity Parity
        {
            get { return Serial.Parity; }
            set { Serial.Parity = value; }
        }

        /// <summary>
        /// 获取或设置每个字节的标准数据位长度，默认为8位
        /// </summary>
        public int DataBits
        {
            get { return Serial.DataBits; }
            set { Serial.DataBits = value; }
        }

        /// <summary>
        /// 获取或设置每个字节的标准停止位数，One，One5，Twe方便表示1、1.5、2个停止位
        /// </summary>
        public StopBits StopBits
        {
            get { return Serial.StopBits; }
            set { Serial.StopBits = value; }
        }

        /// <summary>
        /// 判断串口是否打开
        /// </summary>
        public bool IsOpen { get { return Serial.IsOpen; } }

        /// <summary>
        /// 获取或设置一个值，该值在串行通信过程中启用数据终端就绪 (DTR) 信号
        /// </summary>
        public bool DtrEnable
        {
            get { return Serial.DtrEnable; }
            set { Serial.DtrEnable = value; }
        }

        /// <summary>
        /// 获取或设置一个值，该值指示在串行通信中是否启用请求发送 (RTS) 信号
        /// </summary>
        public bool RtsEnable
        {
            get { return Serial.RtsEnable; }
            set { Serial.RtsEnable = value; }
        }

        /// <summary>
        /// 获取或设置用于解释 ReadLine( )和WriteLine( )方法调用结束的值
        /// </summary>
        public string NewLine
        {
            get { return Serial.NewLine; }
            set { Serial.NewLine = value; }
        }

        /// <summary>
        /// 是否使用接收超时机制
        /// 默认为真
        /// 接收到数据后计时，计时期间收到数据，累加数据，重新开始计时。超时后返回接收到的数据。
        /// </summary>
        public bool ReceiveTimeoutEnable { get; set; } = true;

        /// <summary>
        /// 读取接收数据未完成之前的超时时间
        /// 默认128ms
        /// </summary>
        public int ReceiveTimeout { get; set; } = 128;

        /// <summary>
        /// 超时检查线程运行标志
        /// </summary>
        private bool timeoutCheckThreadIsWork = false;

        /// <summary>
        /// 最后接收到数据的时间点
        /// </summary>
        private int lastReceiveTick = 0;

        /// <summary>
        /// 接到数据的长度
        /// </summary>
        private int receiveDatalen;

        /// <summary>
        /// 接收缓冲区
        /// </summary>
        private byte[] recviceBuffer;

        /// <summary>
        /// 接收缓冲区大小
        /// 默认4K
        /// </summary>
        public int BufSize
        {
            get
            {
                if (recviceBuffer == null)
                    return 4096;
                return recviceBuffer.Length;
            }

            set
            {
                recviceBuffer = new byte[value];
            }
        }

        public CustomSerialPort(string portName, int baudRate = 115200, Parity parity = Parity.None, int databits = 8, StopBits stopBits = StopBits.One)
        {
            Serial = new SerialPortStream
            {
                PortName = portName,
                BaudRate = baudRate,
                Parity = parity,
                DataBits = databits,
                StopBits = stopBits,
            };

            DtrEnable = true;
            RtsEnable = true;
        }

        public static string[] GetPortNames()
        {
            List<string> serailports = new List<string>();
            serailports.AddRange(SerialPortStream.GetPortNames());
            serailports.Sort();
            return serailports.ToArray();
        }

        /// <summary>
        /// 打开串口
        /// </summary>
        /// <returns></returns>
        public bool Open()
        {
            try
            {
                if (recviceBuffer == null)
                {
                    recviceBuffer = new byte[BufSize];
                }

                Serial.Open();
                Serial.DataReceived += Sp_DataReceived; // new SerialDataReceivedEventHandler(sp_DataReceived);
                return true;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 关闭串口
        /// </summary>
        public void Close()
        {
            if (Serial != null && Serial.IsOpen)
            {
                Serial.DataReceived -= Sp_DataReceived; // new SerialDataReceivedEventHandler(sp_DataReceived);
                Serial.Close();
                if (ReceiveTimeoutEnable)
                {
                    Thread.Sleep(ReceiveTimeout);
                    ReceiveTimeoutEnable = false;
                }
            }
        }

        /// <summary>
        /// 释放串口
        /// </summary>
        public void Dispose()
        {
            if (Serial != null)
                Serial.Dispose();
        }

        /// <summary>
        /// 后台线程处理，表示收到串口消息后，触发那些事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int canReadBytesLen = 0;
            if (ReceiveTimeoutEnable)
            {
                while (Serial.BytesToRead > 0)
                {
                    canReadBytesLen = Serial.BytesToRead;
                    if (receiveDatalen + canReadBytesLen > BufSize)
                    {
                        receiveDatalen = 0;
                        throw new Exception("Serial port receives buffer overflow!");
                    }

                    var receiveLen = Serial.Read(recviceBuffer, receiveDatalen, canReadBytesLen);
                    if (receiveLen != canReadBytesLen)
                    {
                        receiveDatalen = 0;
                        throw new Exception("Serial port receives exception!");
                    }

                    // Array.Copy(recviceBuffer, 0, receivedBytes, receiveDatalen, receiveLen);
                    receiveDatalen += receiveLen;
                    lastReceiveTick = Environment.TickCount;
                    if (!timeoutCheckThreadIsWork)
                    {
                        timeoutCheckThreadIsWork = true;
                        Thread thread = new Thread(ReceiveTimeoutCheckFunc)
                        {
                            Name = "ComReceiveTimeoutCheckThread",
                        };
                        thread.Start();
                    }
                }
            }
            else
            {
                if (ReceivedEvent != null)
                {
                    // 获取字节长度
                    int bytesNum = Serial.BytesToRead;
                    if (bytesNum == 0)
                        return;
                    // 创建字节数组
                    byte[] resultBuffer = new byte[bytesNum];

                    int i = 0;
                    while (i < bytesNum)
                    {
                        // 读取数据到缓冲区
                        int j = Serial.Read(recviceBuffer, i, bytesNum - i);
                        i += j;
                    }

                    Array.Copy(recviceBuffer, 0, resultBuffer, 0, i);
                    ReceivedEvent(this, resultBuffer);
                    // System.Diagnostics.Debug.WriteLine("len " + i.ToString() + " " + ByteToHexStr(resultBuffer));
                }

                // Array.Clear (receivedBytes,0,receivedBytes.Length );
                receiveDatalen = 0;
            }
        }

        /// <summary>
        /// 超时返回数据处理线程方法
        /// </summary>
        protected void ReceiveTimeoutCheckFunc()
        {
            while (timeoutCheckThreadIsWork)
            {
                if (Environment.TickCount - lastReceiveTick > ReceiveTimeout)
                {
                    if (ReceivedEvent != null)
                    {
                        byte[] returnBytes = new byte[receiveDatalen];
                        Array.Copy(recviceBuffer, 0, returnBytes, 0, receiveDatalen);
                        ReceivedEvent(this, returnBytes);
                    }

                    // Array.Clear (receivedBytes,0,receivedBytes.Length );
                    receiveDatalen = 0;
                    timeoutCheckThreadIsWork = false;
                }
                else
                {
                    Thread.Sleep(16);
                }
            }
        }

        /// <summary>
        /// 以字节流的方式写入串口
        /// </summary>
        /// <param name="buffer"></param>
        public void Write(byte[] buffer)
        {
            if (IsOpen)
                Serial.Write(buffer, 0, buffer.Length);

            System.Diagnostics.Debug.WriteLine(ByteToHexStr(buffer));
        }

        /// <summary>
        /// 以字符串的形式写入串口
        /// </summary>
        /// <param name="text"></param>
        public void Write(string text)
        {
            if (IsOpen)
                Serial.Write(text);
        }

        /// <summary>
        /// 写入字符串，应该是与Modbus ASCII有关，Ascii方式需要在数据后面加上换行符表示已经结束传送
        /// </summary>
        /// <param name="text"></param>
        public void WriteLine(string text)
        {
            if (IsOpen)
                Serial.WriteLine(text);
        }

        /// <summary>
        /// 把比特流转为字符串
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string ByteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {
                for (int i = 0; i < bytes.Length; i++)
                {
                    returnStr += bytes[i].ToString("X2") + " ";
                }
            }

            return returnStr;
        }
    }

    /// <summary>
    /// 串口接收事件
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="bytes">接收到的数据</param>
    public delegate void CustomSerialPortReceivedEventHandle(object sender, byte[] bytes);
#endif
}
