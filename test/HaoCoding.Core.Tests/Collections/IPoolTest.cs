﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Core.Tests.Collections
{
    using System.Text;
    using HaoCoding.Collections;
    using HaoCoding.Develops;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// 对象池功能测试
    /// </summary>
    public class IPoolTest : TestBase
    {
        private static readonly object Lock = new object();

        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="output">用于提供测试输出的类</param>
        public IPoolTest(ITestOutputHelper output)
            : base(output)
        {
        }

        /// <summary>
        /// 测试字符串构建器池时间
        /// </summary>
        [Fact]
        public void TestStringBuilderPool()
        {
            CodeTimer.Initialize();
            CodeTimer.CodeExecuteTime(() =>
            {
                for (var i = 0; i < 20000; i++)
                {
                    var sb = Pool.StringBuilder.Get();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }

                    sb.Put(true);
                }
            });
        }

        /// <summary>
        /// 测试字符串构建器池时间,多个循环
        /// </summary>
        [Fact]
        public void TestStringBuilderPool1()
        {
            CodeTimer.Initialize();
            CodeTimer.CodeExecuteTime(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    var sb = Pool.StringBuilder.Get();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }

                    sb.Put(true);
                }

                for (var i = 0; i < 10000; i++)
                {
                    var sb = Pool.StringBuilder.Get();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }

                    sb.Put(true);
                }

                for (var i = 0; i < 10000; i++)
                {
                    var sb = Pool.StringBuilder.Get();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }

                    sb.Put(true);
                }
            });
        }

        /// <summary>
        /// 测试字符串构建器时间
        /// </summary>
        [Fact]
        public void TestStringBuilder()
        {
            CodeTimer.Initialize();
            CodeTimer.CodeExecuteTime(() =>
            {
                for (var i = 0; i < 20000; i++)
                {
                    var sb = new StringBuilder();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }
                }
            });
        }

        /// <summary>
        /// 测试字符串构建器时间,多个循环
        /// </summary>
        [Fact]
        public void TestStringBuilder1()
        {
            CodeTimer.Initialize();
            CodeTimer.CodeExecuteTime(() =>
            {
                for (var i = 0; i < 10000; i++)
                {
                    var sb = new StringBuilder();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }
                }

                for (var i = 0; i < 10000; i++)
                {
                    var sb = new StringBuilder();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }
                }

                for (var i = 0; i < 10000; i++)
                {
                    var sb = new StringBuilder();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }
                }
            });
        }

        /// <summary>
        /// 测试字符串构建器时间,多个循环
        /// </summary>
        [Fact]
        public void TestStringBuilder2()
        {
            UnitTester.TestConcurrency(() => CreateStringBuilder(10000), 10);
        }

        /// <summary>
        /// 测试字符串构建器池时间
        /// </summary>
        [Fact]
        public void TestStringBuilderPoo2()
        {
            UnitTester.TestConcurrency(() => CreateStringBuilderPool(10000), 10);
        }

        private void CreateStringBuilder(long length)
        {
            CodeTimer.Initialize();
            CodeTimer.CodeExecuteTime(() =>
            {
                for (int i = 0; i < length; i++)
                {
                    var sb = new StringBuilder();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }
                }
            });
        }

        private void CreateStringBuilderPool(long length)
        {
            CodeTimer.Initialize();
            CodeTimer.CodeExecuteTime(() =>
            {
                for (int i = 0; i < length; i++)
                {
                    var sb = Pool.StringBuilder.Get();
                    for (var j = 0; j < 10000; j++)
                    {
                        sb.Append(j);
                    }

                    sb.Put(true);
                }
            });
        }
    }
}
