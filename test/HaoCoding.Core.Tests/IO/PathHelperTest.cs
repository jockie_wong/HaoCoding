﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Core.Tests.IO
{
    using Xunit.Abstractions;

    /// <summary>
    /// 路径操作帮助测试
    /// </summary>
    public class PathHelperTest : TestBase
    {
        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="output">用于提供测试输出的类</param>
        public PathHelperTest(ITestOutputHelper output)
            : base(output)
        {
        }
    }
}
