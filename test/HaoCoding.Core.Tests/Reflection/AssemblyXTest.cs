﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Core.Tests.Reflection
{
    using System.Reflection;
    using HaoCoding.Reflection;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// 程序集辅助类测试
    /// </summary>
    public class AssemblyXTest : TestBase
    {
        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="output">用于提供测试输出的类</param>
        public AssemblyXTest(ITestOutputHelper output)
            : base(output)
        {
        }

        [Fact]
        private void AssemblyTest()
        {
            var assembly = Assembly.GetExecutingAssembly();
        }

        [Fact]
        private void CompileTest()
        {
            var asm = AssemblyX.Create(Assembly.GetExecutingAssembly());
        }
    }
}
