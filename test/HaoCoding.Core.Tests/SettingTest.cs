﻿// Copyright (c) 深圳云企微商网络科技有限公司. All Rights Reserved.
// 丁川 QQ：2505111990 微信：i230760 qq群:774046050 邮箱:2505111990@qq.com
// Licensed under the Apache License, Version 2.0. See LICENSE in the project root for license information.

namespace HaoCoding.Core.Tests
{
    using System;
    using System.IO;
    using Xunit;
    using Xunit.Abstractions;

    /// <summary>
    /// 核心设置测试
    /// </summary>
    public class SettingTest : TestBase
    {
        /// <summary>
        /// 实例化
        /// </summary>
        /// <param name="output">用于提供测试输出的类</param>
        public SettingTest(ITestOutputHelper output)
            : base(output)
        {
            Setting.Current.ConfigFile = Path.Join(AppContext.BaseDirectory, @"Config\Core1.config");
        }

        /// <summary>
        /// 测试是否启用全局调试
        /// </summary>
        [Fact]
        public void DebugTest()
        {
            var debug = Setting.Current.Debug;
            Assert.True(debug == true);
        }

        /// <summary>
        /// 测试配置文件路径
        /// </summary>
        [Fact]
        public void ConfigFileTest()
        {
            var filepath = Setting.Current.ConfigFile;
            Assert.Equal(filepath, Path.Join(AppContext.BaseDirectory, @"Config\Core1.config"));
        }
    }
}
